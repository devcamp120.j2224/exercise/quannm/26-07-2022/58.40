package com.devcamp.j5840.j5840.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j5840.j5840.model.CProduction;

public interface IProductionRepository extends JpaRepository<CProduction, Long> {
    
}
