package com.devcamp.j5840.j5840;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5840Application {

	public static void main(String[] args) {
		SpringApplication.run(J5840Application.class, args);
	}

}
