package com.devcamp.j5840.j5840.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5840.j5840.model.CProduction;
import com.devcamp.j5840.j5840.repository.IProductionRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CProductionController {
    @Autowired
    IProductionRepository pIProductionRepository;

    @GetMapping("/productions")
    public ResponseEntity<List<CProduction>> getAllProduction() {
        try {
            List<CProduction> lisProductions = new ArrayList<CProduction>();

            pIProductionRepository.findAll().forEach(lisProductions::add);

            return new ResponseEntity<List<CProduction>>(lisProductions, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
